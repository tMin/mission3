//
//  Model.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quote.h"

@interface QuoteManager : NSObject

+(id)shareQuoteManager;
-(Quote *)getQuoteAtIndex:(int)index;
-(void)addQuote:(Quote *)quote;
-(int)returnNumberOfQuotes;
-(QuoteType)getQuoteTypeForQuoteAtIndex:(int)index;
-(NSDictionary *)getQuoteParamaterListAtIndex:(int)index;
@end
