//
//  Model.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "QuoteManager.h"

@interface QuoteManager ()
@property (strong, nonatomic) NSMutableArray *quotes;
@end

@implementation QuoteManager

+(id)shareQuoteManager {
    static QuoteManager *quoteManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        quoteManager = [[self alloc]init];
        quoteManager.quotes = [[NSMutableArray alloc]init];
    });
    return quoteManager;
}

-(Quote *)getQuoteAtIndex:(int)index {
    return self.quotes[index];
}

-(void)addQuote:(Quote *)quote {
    [self.quotes addObject:quote];
}

-(int)returnNumberOfQuotes {
    return (int)[self.quotes count];
}

-(QuoteType)getQuoteTypeForQuoteAtIndex:(int)index {
    return [self.quotes[index] getQuoteType];
}

-(NSDictionary *)getQuoteParamaterListAtIndex:(int)index {
    return [[self getQuoteAtIndex:index] getAttributesList];
}
@end
