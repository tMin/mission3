//
//  LogInViewController.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-16.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "LogInViewController.h"
#import "Constants.h"
#import "KeyboardAnimationHandler.h"
#import <Google/Analytics.h>

@interface LogInViewController () {
    NSUserDefaults *loginDefaults;
    KeyboardAnimationHandler *keyboardAnimationHandler;
    int logInAttempt;
}

@property (strong, nonatomic) IBOutlet UILabel *errorLabel; // should probably change to be alertView
@property (strong, nonatomic) IBOutlet UITextField *userNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;

@end

@implementation LogInViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    [self.errorLabel setPreferredMaxLayoutWidth:200.0];
    keyboardAnimationHandler = [[KeyboardAnimationHandler alloc]initWithViewController:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    logInAttempt = 0;
    [self.navigationController setNavigationBarHidden:YES];
    [keyboardAnimationHandler registerForKeyboardNotifications];
    
    //GA screen tracking
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"LogInViewController"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [keyboardAnimationHandler deregisterFromKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark validations

-(bool)validateEmail:(NSString *)email {
    if ([email length] == 0) {
        return 1;
    }
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", Email_Reg_Ex];
    return ![emailTest evaluateWithObject:email];
}

-(bool)password:(NSString *)password mustEqualConfirmedPassword:(NSString *)confirmedPassword {
    if ([self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        return true;
    }
    return false;
}

-(bool)validPassword:(NSString *)password {
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Password_Reg_Ex];
    return [passwordTest evaluateWithObject:self.confirmPasswordTextField.text];
}

# pragma mark Gesture Recognizers

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view.window endEditing:YES];
}

- (IBAction)tappedLogIn:(id)sender {
    loginDefaults = [NSUserDefaults standardUserDefaults];
    logInAttempt ++;
    
    // validation
    self.errorLabel.text = @"";
    
    if ([self.userNameTextField.text length] == 0) {
        self.errorLabel.text = [self.errorLabel.text stringByAppendingString:@"User Name must not be blank. "];
    }
    if (![self password:self.passwordTextField.text mustEqualConfirmedPassword:self.confirmPasswordTextField.text]) {
        self.errorLabel.text = [self.errorLabel.text stringByAppendingString:@"Password and confirmed password must be equal. "];
    }
    if (![self validPassword:self.passwordTextField.text]) {
        self.errorLabel.text = [self.errorLabel.text stringByAppendingString:@"Password must be 4 to 8 characters long and include at least one number, one lower case, and one upper case letter. "];
    }

    if ([self validateEmail:self.emailTextField.text]) {
        self.errorLabel.text = [self.errorLabel.text stringByAppendingString:@"Invalid email. "];
    }
    
    // if no errors
    if ([self.errorLabel.text length] == 0) {
        //store data in NSUSERDefaults
        [loginDefaults setValue:self.userNameTextField.text forKey:Username_Key];
        [loginDefaults setValue:self.passwordTextField.text forKey:Password_Key];
        [loginDefaults setValue:self.emailTextField.text forKey:Email_Key];
        
        // GA event tracking
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Log In"
                                                              action:@"Success"
                                                               label:@"website"
                                                               value:[NSNumber numberWithInt:logInAttempt]] build]];
        
        [self performSegueWithIdentifier:@"unwindFromLoginSegue" sender:self];
    } else {
        // GA event tracking
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Log In"
                                                              action:@"Unsuccessful"
                                                               label:self.errorLabel.text
                                                               value:[NSNumber numberWithInt:logInAttempt]] build]];
    }
}

@end
