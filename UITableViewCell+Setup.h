//
//  UITableViewCell+Setup.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Quote.h"

@interface UITableViewCell (setup) 

-(void)configCellWithDictionary:(NSDictionary *)paramaters;

@end
