//
//  CellFactory.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "UITableViewCell+Setup.h"
#import "IOSQuoteCell.h"
#import "WebsiteQuoteCell.h"
#import "CellFactory.h"

@interface CellFactory () {
        UITableViewCell *cell;
}

@end

@implementation CellFactory
- (UITableViewCell *)createCell:(UITableView *)tableView forIndexPath:indexPath forType:(QuoteType)type {
    
    NSLog(@"CellFactory: Type to create: %d", (int)type);
    
    if (type == IOSQuote) {
        cell = [[IOSQuoteCell alloc] initWithTableView:tableView forIndexPath:indexPath];
    } else if (type == WebsiteQuote) {
        cell = [[WebsiteQuoteCell alloc] initWithTableView:tableView forIndexPath:indexPath];
    }
    
    return cell;
}
@end
