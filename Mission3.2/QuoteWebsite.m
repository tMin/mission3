//
//  Quote2.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "QuoteWebsite.h"

@implementation QuoteWebsite

-(QuoteType)getQuoteType {
    return WebsiteQuote;
}

-(NSDictionary *)getAttributesList {
    NSDictionary *attributes = @{
                                 @"title": self.title,
                                 @"description": self.quoteDescription,
                                 @"quoteNumber": [self.myQuoteNumber stringValue]
                                 };
    return attributes;
}
@end
