//
//  Quote2Cell.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewController.h"

@interface WebsiteQuoteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *quoteNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;

-(id)initWithTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath;

@end
