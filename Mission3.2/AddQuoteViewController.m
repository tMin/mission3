//
//  AddQuoteViewController.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "AddQuoteViewController.h"
#import "TableViewController.h"
#import "QuoteManager.h"
#import <Google/Analytics.h>

@interface AddQuoteViewController () {
    QuoteManager *quoteManager;
    KeyboardAnimationHandler *keyboardHandler;
}
@end

@implementation AddQuoteViewController


-(void)viewDidLoad {
    [super viewDidLoad];
    quoteManager = [QuoteManager shareQuoteManager];
    keyboardHandler = [[KeyboardAnimationHandler alloc]initWithViewController:self];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [keyboardHandler registerForKeyboardNotifications];

    //GA screen tracking
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"AddQuoteViewController"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [keyboardHandler deregisterFromKeyboardNotifications];
}


#pragma mark -IBActions

- (IBAction)Quote1Button:(id)sender {
    // need to be refactored. add quote method should probably just take the quote type and string, and the quote manager should instanciate the quote (AKA this code should be in the quote manager)
    QuoteIOS *iOSQuote = [[QuoteIOS alloc]init];
    iOSQuote.title = self.Quote1TextField.text;
    [quoteManager addQuote:iOSQuote];
    
    // GA event tracking
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Quotes"
                                                          action:@"Create New Quote"
                                                           label:@"iOS"
                                                           value:nil] build]];
}

- (IBAction)Quote2Button:(id)sender {
     // need to be refactored. add quote method should probably just take the quote type and string, and the quote manager should instanciate the quote (AKA this code should be in the quote manager)
    QuoteWebsite *webQuote = [[QuoteWebsite alloc]init];
    webQuote.title = self.Quote2TextField.text;
    webQuote.quoteDescription = self.Quote2TextField2.text;
    [quoteManager addQuote:webQuote];
    
    // GA event tracking
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Quotes"
                                                          action:@"Create New Quote"
                                                           label:@"website"
                                                           value:nil] build]];
}

# pragma Mark - ResignFirstResponder
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view.window endEditing:YES];
}

@end
