//
//  AddQuoteViewController.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Quote.h"
#import "QuoteIOS.h"
#import "QuoteWebsite.h"
#import "KeyboardAnimationHandler.h"

@interface AddQuoteViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *Quote1TextField;
@property (strong, nonatomic) IBOutlet UITextField *Quote2TextField;
@property (strong, nonatomic) IBOutlet UITextField *Quote2TextField2;

@end
