//
//  Quote1Cell.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewController.h"

@interface IOSQuoteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *quoteNumberLabel;

-(id)initWithTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath;
@end
