//
//  KeyboardHandler.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-18.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KeyboardAnimationHandler : NSObject

-(KeyboardAnimationHandler *)initWithViewController:(UIViewController *)vc;
-(void)registerForKeyboardNotifications;
-(void)deregisterFromKeyboardNotifications;

@end
