//
//  TableViewController.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "TableViewController.h"
#import "AddQuoteViewController.h"
#import "Quote.h"
#import "QuoteIOS.h"
#import "QuoteWebsite.h"
#import "CellFactory.h"
#import "IOSQuoteCell.h"
#import "WebsiteQuoteCell.h"
#import "UITableViewCell+Setup.h"
#import "AddQuoteViewController.h"
#import "QuoteManager.h"
#import "Constants.h"
#import <Google/Analytics.h>

@interface TableViewController () {
    CellFactory *cellFactory;
    QuoteManager *quoteManager;
    NSUserDefaults *loginDefaults;
}

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //check if user has logged in, if not send to sign up 
    loginDefaults = [NSUserDefaults standardUserDefaults];
    if (![loginDefaults objectForKey:Username_Key]) {
        [self performSegueWithIdentifier:@"SignUpSegue" sender:self];
    }

    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    cellFactory = [[CellFactory alloc] init];
    quoteManager =[QuoteManager shareQuoteManager];
    
    self.tableView.estimatedRowHeight = 100;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated {
    //GA screen tracking
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"TableViewController"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -Navigation

-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue{
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [quoteManager returnNumberOfQuotes];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger modelType = [quoteManager getQuoteTypeForQuoteAtIndex:(int)indexPath.row];
    UITableViewCell *cell = [cellFactory createCell:tableView forIndexPath:indexPath forType:modelType];
    
    [cell configCellWithDictionary:[quoteManager getQuoteParamaterListAtIndex:(int)indexPath.row]];
    
    return cell;
}

#pragma mark - Navigation

- (IBAction)unwindFormLogInSegue:(UIStoryboardSegue *)unwindSegue {
    
}

@end
