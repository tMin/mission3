//
//  Quote1.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "QuoteIOS.h"
@interface QuoteIOS ()
@end

@implementation QuoteIOS

-(QuoteType)getQuoteType {
    return IOSQuote;
}

-(NSDictionary *)getAttributesList {
    NSDictionary *attributes = @{
                                 @"title": self.title,
                                 @"quoteNumber": [self.myQuoteNumber stringValue]
                                 };
    return attributes;
}
@end
