//
//  TableViewController.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Quote.h"
#import "KeyboardAnimationHandler.h"


@interface TableViewController : UITableViewController <UITableViewDataSource, UITabBarDelegate>

@property (strong, nonatomic)KeyboardAnimationHandler *keyboardHandler;

@end
