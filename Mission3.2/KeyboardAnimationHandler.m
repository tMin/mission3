//
//  KeyboardHandler.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-18.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "KeyboardAnimationHandler.h"

@interface KeyboardAnimationHandler ()

@property (strong, nonatomic)UIViewController *viewController;

@end

@implementation KeyboardAnimationHandler

-(KeyboardAnimationHandler *)initWithViewController:(UIViewController *)vc {
    self = [super init];
    self.viewController = vc;
    return self;
}

-(void)keyboardFrameWillChange:(NSNotification *)notification {
    CGRect keyboardBeginFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect newFrame = self.viewController.view.frame;
    CGRect keyboardFrameEnd = [self.viewController.view convertRect:keyboardEndFrame toView:nil];
    CGRect keyboardFrameBegin = [self.viewController.view convertRect:keyboardBeginFrame toView:nil];

    [UIView beginAnimations:nil context:nil];
    
    if ((keyboardFrameBegin.size.height - keyboardFrameEnd.size.height) != 0 ) {
        // quicktype bar appeared or disappeard
        newFrame.size.height += (keyboardFrameBegin.size.height - keyboardFrameEnd.size.height);
        self.viewController.view.frame = newFrame;
        
    } else {
        // keyboard appeared or diapeared
        UIViewAnimationCurve animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
        NSTimeInterval animationDuration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:animationCurve];
        
        newFrame.size.height -= (keyboardFrameBegin.origin.y - keyboardFrameEnd.origin.y);
        self.viewController.view.frame = newFrame;
    }
    [UIView commitAnimations];
}

#pragma mark - keyboard handling

-(void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardFrameWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
}

@end
