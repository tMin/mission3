//
//  Quote.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum quoteType {
    IOSQuote,
    WebsiteQuote
}QuoteType;


@interface Quote : NSObject

@property (nonatomic) NSNumber *myQuoteNumber;
@property (strong, nonatomic) NSString *title;
+(void)setQuoteNumber;
-(NSDictionary *)getAttributesList;
@end

@protocol quoteChild
@required
-(QuoteType)getQuoteType;
-(NSDictionary *)getAttributesList;
@end