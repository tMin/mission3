//
//  Quote.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "Quote.h"


static NSNumber *quoteNumber;

@implementation Quote

+(void)setQuoteNumber{
    if (quoteNumber == nil) { // could use gcd & once token to fix any possible multi threading issues 
        quoteNumber = [NSNumber numberWithInt:1];
    } else {
        quoteNumber = [NSNumber numberWithInt:[quoteNumber intValue]+1];
    }
}

-(id)init {
    if ([self isMemberOfClass:[Quote class]]) {
        // if init was called on a Quote class, not a child of the Quote class run time error is thrown.
        NSLog(@"init called on Quote.m object directly");
        [self doesNotRecognizeSelector:_cmd];
        return nil;
    } else {
        // init called by child of Quote class 
        Quote <quoteChild> *selftemp = [super init]; // checks that quoteChild Protocal implemented
        self = selftemp;
        [Quote setQuoteNumber];
        self.myQuoteNumber = [NSNumber numberWithInt: [quoteNumber intValue]];
        return self;
    }
}

@end
