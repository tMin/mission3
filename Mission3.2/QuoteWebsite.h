//
//  Quote2.h
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quote.h"

@interface QuoteWebsite : Quote <quoteChild>
    @property (strong, nonatomic) NSString *quoteDescription;
@end
