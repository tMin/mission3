//
//  Quote1Cell.m
//  Mission3.2
//
//  Created by thomas minshull on 2015-11-08.
//  Copyright © 2015 Tom m. All rights reserved.
//

#import "IOSQuoteCell.h"

@implementation IOSQuoteCell

-(id)initWithTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath {
    self = [super init];
    
    if (self) {
        return [tableView dequeueReusableCellWithIdentifier:@"Quote1" forIndexPath:indexPath];
    }
    return self;
}

-(void)configCellWithDictionary:(NSDictionary *)paramaters {
    self.titleLabel.text = [paramaters objectForKey:@"title"];
    self.quoteNumberLabel.text = [paramaters objectForKey: @"quoteNumber"];
};

@end
